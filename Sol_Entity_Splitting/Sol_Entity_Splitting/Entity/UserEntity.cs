﻿using Sol_Entity_Splitting.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Entity_Splitting.Entity
{
    public class UserEntity: IUserEntity
    {
        public decimal? UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public IUserLoginEntity userLoginEntityObj { get; set; }

        public IUserCommunicationEntity userCommunicationEntity { get; set; }
    }
}
