﻿using Sol_Entity_Splitting.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Entity_Splitting.Entity
{
    public class UserLoginEntity: IUserLoginEntity
    {
        public decimal? UserId { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
