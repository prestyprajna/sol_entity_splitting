﻿using Sol_Entity_Splitting.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Entity_Splitting.Entity
{
    public class UserCommunicationEntity: IUserCommunicationEntity
    {
        public decimal? UserId { get; set; }

        public string MobileNo { get; set; }

        public string EmailId { get; set; }
    }
}
