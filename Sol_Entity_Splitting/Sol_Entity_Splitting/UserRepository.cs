﻿using Sol_Entity_Splitting.EF;
using Sol_Entity_Splitting.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Entity_Splitting
{
    public class UserRepository
    {
        #region  declaration

        private UserDBEntities db = null;

        #endregion

        #region  constructor

        public UserRepository()
        {
            db = new UserDBEntities();
        }

        #endregion

        #region  public methods

        public async Task<IEnumerable<UserEntity>> GetSelectData(UserEntity userEntityObj)
        {
            try
            {
                return await Task.Run(() =>
                {
                    var getQuery =
                    db
                    ?.tblUsers
                    ?.AsEnumerable()
                    ?.Select(this.GetData)
                    ?.ToList();


                    return getQuery;
                });
            }
            catch (Exception)
            {

                throw;
            }
           
        }

       

        #endregion


        #region   private method

        private Func<tblUser,UserEntity> GetData
        {
            get
            {
                return
                    (leTblUser) => new UserEntity()
                    {
                        UserId = leTblUser?.UserId ?? null,
                        FirstName = leTblUser?.FirstName ?? null,
                        LastName = leTblUser?.LastName ?? null,
                        userLoginEntityObj = new UserLoginEntity()
                        {
                            Username = leTblUser?.Username ?? null,
                            Password = leTblUser?.Password ?? null
                        },
                        userCommunicationEntity = new UserCommunicationEntity()
                        {
                            MobileNo = leTblUser?.MobileNo ?? null,
                            EmailId = leTblUser?.EmailId ?? null
                        }
                    };
            }
        }

        #endregion
    }
}
