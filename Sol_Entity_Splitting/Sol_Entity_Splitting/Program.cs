﻿using Sol_Entity_Splitting.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Entity_Splitting
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async() =>
            {
                try
                {
                    #region  select 
                    IEnumerable<UserEntity> userEntityObj = await new UserRepository().GetSelectData(null);

                    foreach (UserEntity val in userEntityObj)
                    {
                        Console.WriteLine(val.UserId);
                        Console.WriteLine(val.FirstName);
                        Console.WriteLine(val.LastName);
                        Console.WriteLine(val.userLoginEntityObj.Username);
                        Console.WriteLine(val.userLoginEntityObj.Password);
                        Console.WriteLine(val.userCommunicationEntity.MobileNo);
                        Console.WriteLine(val.userCommunicationEntity.EmailId);
                    }

                    #endregion


                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
               
            }).Wait();
        }
    }
}
